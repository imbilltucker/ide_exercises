# IDE tasks to show
### Consider using screenkey to show commands on the screen
#####  https://gitlab.com/wavexx/screenkey

    * Find and replace,  (lorem_ipsum.txt)
        - search and replace all
        - walk thru all matches and choose whether to change it or not
        - do it thru multiple files

    * File rename (lorem_ipsum.txt --> loremissimum_ipsum.txt)

    * Complex multiline edits, (variables.js, list_of_tags.html)
        - large files
        - within tags
        - common tasks:
            - add semi-colons on the end
            - sort lists

    * Formatting
        - format file  (example.sql, example.xml)
        - format code, q.v. code examples in Gilded Rose Kata

    * Following a code heirarchy  (choose favorite code example from Gilded Rose Kata
        - return to a previous place in code
        - go to where variable is defined

    * Getting definitions
        - documentation for code (doc strings?  official docs?  available functions?)

    *  TOP THREE favorite features.
        1.
        1.
        1.


    * Bonus Tasks:
        - show most recently used files
        - file and directory interface (or options thereof)
        - version control repo interface
